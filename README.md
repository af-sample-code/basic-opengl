This repository contains some sample codes used to introduce OpenGL in CG1-NPGR003 at Charles University. It is solely meant for educational purpose, use at your own risk.

The different examples are stored on specific branches:

- `1_simple_triangle` shows how to send vertex data to the GPU and render a triangle with a basic vertex and fragment shader.
- `2_colored_triangle` add colors to the edges of the triangle. It shows how fragments are interpolated from vertices.
- `3_cube` introduces element buffer objects (EBOs) to draw more efficiently meshes using multiple times the same vertex. It also shows how to send a matrix to a shader.
